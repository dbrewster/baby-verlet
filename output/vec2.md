# Global





* * *

## Class: Vec2


### Vec2.add(v) 

Calculate the sum of this and another vector

**Parameters**

**v**: `Vec2`, Second vector

**Returns**: `Vec2`, Sum

### Vec2.subtract(v) 

Calculate the difference between this and another vector

**Parameters**

**v**: `Vec2`, Second vector

**Returns**: `Vec2`, Difference

### Vec2.dotProduct(v) 

Calculate the dot product of this and another vector

**Parameters**

**v**: `Vec2`, Second vector

**Returns**: `Number`, Product

### Vec2.scale(i) 

Scale this vector

**Parameters**

**i**: `Number`, Scale factor

**Returns**: `Vec2`, Scaled vector

### Vec2.clone() 

Create a new vector with the same values as this one

**Returns**: `Vec2`, New vector

### Vec2.normalize() 

Normalize this vector

**Returns**: `Vec2`, Normalized vector

### Vec2.getLength() 

Calculate length

**Returns**: `Number`, Length

### Vec2.set(arr) 

Set x and y values from array

**Parameters**

**arr**: `Array`, Array of new values


### Vec2.from(input) 

Create new Vec2 from supplied input

**Parameters**

**input**: `Object | Vec2 | Array`, Data to create object from

**Returns**: `Vec2`, Created vector



* * *










