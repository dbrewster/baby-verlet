/**
 * @class Constraint
 * @param {object} opts Constraint configuration
 * @param {array} opts.particles Constrained particles
 * @param {number} opts.stiffness Stiffness of constraint
 * @param {number} opts.length Length of constraint
 */
var Constraint = function (opts) {
  if (!opts.particles || opts.particles.length < 2) {
    throw new Error('Constraint requires two particles');
  }

  if (opts.stiffness && (opts.stiffness > 1 || opts.stiffness < 0)) {
    throw new Error('Stiffness must be a number between 0 and 1');
  }

  opts = merge({
    stiffness: 1,
    length: this.particles[0].distanceFrom(this.particles[1])
  }, opts]);

  this.particles = opts.particles;
  this.stiffness = opts.stiffness;
  this.length = opts.length;
};

/**
 * Move constrained particles to within bounds
 */
Constraint.prototype.solve = function () {
  var p1 = this.particles[0];
  var pos1 = p1.position;

  var p2 = this.particles[1];
  var pos2 = p2.position;

  var delta = pos2.subtract(pos1);
  var particleDistance = Math.sqrt(delta.dotProduct(delta));
  var diff = (particleDistance - this.length) / particleDistance;
  diff = diff * this.stiffness;

  if (p1.fixed && p2.fixed) {
    return;
  } else if (p1.fixed) {
    p2.position = pos2.subtract(delta.scale(diff));
  } else if (p2.fixed) {
    p1.position = pos1.add(delta.scale(diff));
  } else {
    p1.position = pos1.add(delta.scale(0.5).scale(diff));
    p2.position = pos2.subtract(delta.scale(0.5).scale(diff));
  }
};

module.exports = Constraint;
