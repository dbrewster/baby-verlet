var Vec2 = require('baby-verlet/lib/vec2');

/**
 * @class Attractor
 * @param {[type]} opts [description]
 */
var Attractor = function (opts) {
  opts = merge({
    strength: 1,
    radius: 300
  }, opts);

  this.position = Vec2.from(opts.position);
  this.strength = opts.strength;
  this.radius = opts.radius;
};

module.exports = Attractor;
