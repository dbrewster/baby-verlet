/**
 * @class Vec2
 * @param {Number} x X value
 * @param {Number} y Y value
 */
var Vec2 = function (x, y) {
  this.x = x || 0; 
  this.y = y || 0;
};

/**
 * Calculate the sum of this and another vector
 * @param {Vec2} v Second vector
 * @return {Vec2} Sum
 */
Vec2.prototype.add = function (v) {
  return new Vec2(this.x + v.x, this.y + v.y);
};

/**
 * Calculate the difference between this and another vector
 * @param  {Vec2} v Second vector
 * @return {Vec2} Difference
 */
Vec2.prototype.subtract = function (v) {
  return new Vec2(this.x - v.x, this.y - v.y);
};

/**
 * Calculate the dot product of this and another vector
 * @param  {Vec2} v Second vector
 * @return {Number} Product
 */
Vec2.prototype.dotProduct = function (v) {
  return this.x * v.x + this.y * v.y;
};

/**
 * Scale this vector
 * @param  {Number} i Scale factor
 * @return {Vec2} Scaled vector
 */
Vec2.prototype.scale = function (i) {
  return new Vec2(this.x * i, this.y * i);
};

/**
 * Create a new vector with the same values as this one
 * @return {Vec2} New vector
 */
Vec2.prototype.clone = function () {
  return new Vec2(this.x, this.y);
};

/**
 * Normalize this vector
 * @return {Vec2} Normalized vector
 */
Vec2.prototype.normalize = function () {
  var length = this.getLength();
  return new Vec2(this.x / length, this.y / length);
};

/**
 * Calculate length
 * @return {Number} Length
 */
Vec2.prototype.getLength = function () {
  return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2));
};

/**
 * Set x and y values from array
 * @param {Array} arr Array of new values
 */
Vec2.prototype.set = function (arr) {
  this.x = arr[0];
  this.y = arr[1];
};

/**
 * Create new Vec2 from supplied input
 * @param {Object|Vec2|Array} input Data to create object from
 * @return {Vec2} Created vector
 */
Vec2.from = function (input) {
  if (input && input.constructor === Array) {
    if (0 in input && 1 in input) {
      return new Vec2(input[0], input[1]);
    } else {
      throw new Error('Vector input missing required property');
    }
  } else if (input instanceof Vec2) {
    return input;
  } else if (typeof input === "object") {
    if ('x' in input && 'y' in input) {
      return new Vec2(input.x, input.y);
    } else {
      throw new Error('Vector input missing required property');
    }
  } else if (!input) {
    return new Vec2();
  }
};

module.exports = Vec2;
