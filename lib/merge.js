/**
 * Merge one or more objects
 * @param {...Object} Objects to merge
 * @return {Object} Merged object
 */
var merge = function () {
  var toMerge = Array.prototype.slice.call(arguments, 0);
  var merged = toMerge[0];

  for (var i = 1; i < toMerge.length; ++i) {
    var current = toMerge[i];

    for (var key in current) {
      merged[key] = current[key];
    }
  }

  return merged;
}

module.exports = merge;
