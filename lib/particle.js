var Vec2 = require('./vec2'),
  merge = require('./merge'),
  util = require('./util');

/**
 * @class Particle
 * @param {object} opts particle options
 * @param {vec2} opts.position
 * @param {vec2} opts.previousPosition Previous position
 * @param {boolean} fixed Whether this particle can be moved
*/
var Particle = function (opts) {
  opts = merge({
    previousPosition: opts.position
    fixed: false
  }, opts);

  this.previousPosition = Vec2.from(opts.previousPosition);
  this.position = Vec2.from(opts.position);
  this.fixed = opts.fixed
};

/**
 * Calculates distance between this and another particle
 * @param  {Particle} p2 particle to calculate distance to
 * @return {Vec2} Calculated distance
 */
Particle.prototype.distanceFrom = function (p2) {
  return util.pointDistance(this.position, p2.position);
};

module.exports = Particle;